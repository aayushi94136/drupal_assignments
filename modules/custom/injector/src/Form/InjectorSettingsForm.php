<?php
namespace Drupal\injector\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure injector settings for this site.
 */
class InjectorSettingsForm extends ConfigFormBase {

  /** 
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'injector.admin_settings';

  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'injector_admin_settings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

  $form['injector_select'] = [
  '#type' => 'select',
  '#title' => $this
    ->t('Assets Type'),
  '#options' => [
    'default' => $this
      ->t('Default'),
    'custom' => $this
      ->t('Custom'),
  ],
];
    $form['other_things'] = [
      '#type' => 'textarea',
      '#format'=> 'full_html',
      '#title' => $this->t('Assets Paths'),
      '#default_value' => $config->get('other_things'),
    ];  

    return parent::buildForm($form, $form_state);
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('injector_select', $form_state->getValue('injector_select'))
      // You can set multiple configurations at once by making
      // multiple calls to set().
      ->set('other_things', $form_state->getValue('other_things'))
      ->save();

    parent::submitForm($form, $form_state);   
  }
}